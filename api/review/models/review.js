"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(result, data) {

    },

    async beforeCreate(data) {
      // Добавляем имя и фото профиля к отзыву
      const profileId = data.profile;
      const profile = await strapi.services.profile.findOne({id: profileId});
      if (profile.photo || profile.photo_url) {
        data.photo_url = profile.photo?.formats?.small?.url || profile.photo?.url || profile.photo_url;
      }

      data.name = profile.name;
      // =====================================
    },

    async afterDelete(result, params) {

    },
  },
};
