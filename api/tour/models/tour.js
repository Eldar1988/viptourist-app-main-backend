"use strict";
const tourServices = require("../services/tour");
const bot = require('../../utils/telegram-bot')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */
let counter = 0
module.exports = {
  lifecycles: {

    async beforeCreate(data) {
      if (data.city) {
        const cityId = data.city,
          country = await strapi.services.country.findOne({ id: cityId });
        if (country) {
          data.country = country.name;
        }
      }
      if (!data.vid) {
        const date = new Date();
        const vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`;
        data.vid = vid;
      }
    },

    async beforeUpdate(params, data) {
      const tourToUpdate = await strapi.services.tour.findOne({ id: params?._id || data.id})
      if (tourToUpdate && !tourToUpdate.approved && data.approved && tourToUpdate.createdLanguage === tourToUpdate.locale) {
        if (!tourToUpdate.localizations?.length) {
          // bot.translate.sendMessage('tours', params?._id || data.id, 'New tour: ')
          return tourServices.createTranslations(data, params);
        }
      }
      await tourServices.createApprovedNotification(params, data) // Создание уведомления гиду
    },

    afterUpdate(result, params, data) {
      if(result.createdLanguage === result.locale)  {
        if (data.tourUpdated) {
          bot.admin.sendMessage('tours', params._id, 'Тур был обновлен: ')
        }
      }
    },

    async afterDelete(params, result) {
      if (params) {
        try {
          params.localizations.forEach((item) => {
            strapi.services.tour.delete({ id: item._id });
          });
        } catch (e) {
          throw e;
        }
      }
    },

    async afterCreate(result, data) {
      if (!data.localizations?.length) {
        bot.admin.sendMessage('tours', result.id)
      }
    }

  },
};
