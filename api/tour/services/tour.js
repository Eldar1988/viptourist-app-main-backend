"use strict";
const languages = require("../../utils/languages");
const axios = require("axios");
const translator = require("../../utils/translator");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

let notificationSendStarted = false

module.exports = {
  createTranslations: async function (data, params, iteration = 0) {
    const tour = JSON.parse(JSON.stringify(data));

    await languages.getLanguages()

    const langs = languages.languages.filter(
      (item) => item.locale !== tour.createdLanguage
    );
    const lang = langs[iteration];
    const currentCity = await strapi.services.city.findOne({
      id: tour.city,
    });

    /* ------------------------------------------------ Get translations ------------------------------------------------ */
    let translations = await translator.translateText({
      text: [
        data.name,
        data.description,
        data.languages,
        data.prerequisites,
        data.prohibitions,
        data.included,
        data.not_included,
        data.note,
      ],
      target: lang.short,
    });
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ---------------------------------------------- Get current tour city --------------------------------------------- */
    try {
      tour.city = await axios
        .get(`http://${process.env.HOST}:${process.env.PORT}/cities`, {
          params: {
            vid: currentCity.vid,
            _locale: lang.locale,
          },
        })
        .then((response) => response.data[0].id);
    } catch (e) {
      throw e.message;
    }

    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    /* ------------------------------------------------ Create translate ------------------------------------------------ */
    try {
      await axios
        .post(
          `http://${process.env.HOST}:${process.env.PORT}/tours/${params._id}/localizations`,
          {
            ...tour,
            name: translations?.length ? translations[0] : tour.name,
            description: translations?.length ? translations[1] : tour.description,
            languages: translations?.length ? translations[2] : tour.languages,
            prerequisites: translations?.length ? translations[3] : tour.prerequisites,
            prohibitions: translations?.length ? translations[4] : tour.prohibitions,
            included: translations?.length ? translations[5] : tour.included,
            not_included: translations?.length ? translations[6] : tour.not_included,
            note: translations?.length ? translations[7] : tour.note,
            next: true,
            locale: lang.locale,
          }
        )
        .then((response) => response);
    } catch (e) {
      throw e;
    }
    /* ------------------------------------------------------- /// ------------------------------------------------------ */

    if (iteration !== langs.length - 1) {
      return this.createTranslations(data, params, (iteration += 1));
    } else {
      return null;
    }
  },

  async createApprovedNotification (params, data) {
    const tour = await strapi.services.tour.findOne({ id: params._id })

    if (!tour.approved && data.approved && !notificationSendStarted) {
      notificationSendStarted = true
      const profile = await strapi.services.profile.findOne({ _id: data.profile })
      const pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
        params: {
          _uid: profile.uid
        }
      }).then(res => res.data[0] || null)
      if (!pushProfile) return console.log('Profile for push not found...')

      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: "tourApproved",
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (e) {
        throw e
      }

      setTimeout(() => {
        notificationSendStarted = false
      }, 1000)
    }

  }
}

