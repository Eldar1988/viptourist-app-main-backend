"use strict";
const { sanitizeEntity } = require("strapi-utils");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  // async find(ctx) {
  //   let entities;
  //   if (ctx.query._q) {
  //     entities = await strapi.services.tour.search(ctx.query);
  //   } else {
  //     entities = await strapi.services.tour.find(ctx.query);
  //   }
  //   const sanitizeEntities = entities.map((entity) =>
  //     sanitizeEntity(entity, { model: strapi.models.tour })
  //   );
  //   return sanitizeEntities.map((item) => {
  //     console.log(item.reviews)
  //     return {
  //       id: item.id,
  //       vid: item.vid,
  //       name: item.name,
  //       description: item.description,
  //       location_point: item.location_point,
  //       image_urls: item.image_urls,
  //       country: item.country,
  //       city: {
  //         id: item.city?.id,
  //         name: item.city?.name,
  //         image: item.city?.image?.formats?.medium?.url || ''
  //       },
  //       image: item.image[0]?.formats.small.url,
  //       rating: item.reviews.length ? item.reviews.reduce((prev, current) => prev.rating + current.rating) / item.reviews.length : 0,
  //       reviews_count: item.reviews.length,
  //       price: item.price,
  //       localizations: item.localizations,
  //       locale: item.locale,
  //       createdLanguage: item.createdLanguage,
  //       translationApproved: item.translationApproved,
  //       approved: item.approved,
  //
  //       top: item.top,
  //       guide: item.guide,
  //       private: item.private,
  //       one_day_trip: item.one_day_trip,
  //       nature: item.nature,
  //       ticket_must_have: item.ticket_must_have,
  //       on_water: item.on_water,
  //       package_tour: item.package_tour,
  //       small_group: item.small_group,
  //       invalid_friendly: item.invalid_friendly,
  //       history: item.history,
  //       world_war: item.world_war,
  //       open_air: item.open_air,
  //       street_art: item.street_art,
  //       adrenaline: item.adrenaline,
  //       architecture: item.architecture,
  //       food: item.food,
  //       music: item.music,
  //       for_couples_activities: item.for_couples_activities,
  //       for_kids_activities: item.for_kids_activities,
  //       museum: item.museum,
  //       memorial: item.memorial,
  //       park: item.park,
  //       gallery: item.gallery,
  //       square: item.square,
  //       theater: item.theater,
  //       castle: item.castle,
  //       towers: item.towers,
  //       airports: item.airports,
  //       bicycle: item.bicycle,
  //       minivan: item.minivan,
  //       public_transport: item.public_transport,
  //       limousine: item.limousine,
  //       bicycle_taxi: item.bicycle_taxi,
  //       car: item.car,
  //       cruise: item.cruise,
  //       hunting: item.hunting,
  //       adventure: item.adventure,
  //       fishing: item.fishing,
  //       night: item.night,
  //       game: item.game,
  //       onlyTransfer: item.onlyTransfer,
  //       fewDaysTrip: item.fewDaysTrip,
  //       placesCount: item.placesCount,
  //       date: item.date,
  //       remark: item.remark,
  //     };
  //   });
  // },
};
