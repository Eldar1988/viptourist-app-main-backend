'use strict';
const services = require('../services/city')
const bot = require('../../utils/telegram-bot')

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    beforeCreate(data) {
      services.createVid(data)
    },
    async afterCreate(result, data) {
      await services.createTranslations(result, data)
    },
    async afterDelete(params, result) {
      if (params) {
        try {
          params.localizations.forEach((item) => {
            strapi.services.city.delete({ id: item._id });
          });
        } catch (e) {
          throw e;
        }
      }
    },
  }
};
