'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

const languages = require("../../utils/languages");
const axios = require("axios");
const translator = require("../../utils/translator");
module.exports = {
  async createTranslations (result, data) {

    if (!data.next) {
      await languages.getLanguages()

      const langs = languages.languages.filter(
        (item) => item.locale !== result.createdLanguage
      );

      const city = { ...data };
      let country = {};

      for (const lang of langs) {
        city.locale = lang.locale;

        try {
          country = await axios
            .get(`http://${process.env.HOST}:${process.env.PORT}/countries`, {
              params: {
                vid: result.country.vid,
                _locale: lang.locale,
              },
            })
            .then((response) => response.data[0]);
        } catch (e) {
          throw e;
        }

        let translations = await translator.translateText({
          text: [city.name],
          target: lang.short
        })

        try {
          await axios
            .post(`http://${process.env.HOST}:${process.env.PORT}/cities/${result.id}/localizations`,
              {
                name: translations[0],
                locale: lang.locale,
                country: country.id,
                next: true
              })
        } catch (e) {
          throw e.message
        }
      }
    }
  },
  createVid (data) {
    if (!data.vid) {
      const date = new Date
      data.vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`
    }
  },
};
