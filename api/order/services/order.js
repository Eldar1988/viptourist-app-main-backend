'use strict';
const qr = require("qrcode");
const axios = require("axios");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */


module.exports = {
  // Создание QR кода билета
  async createQR(results, data) {
    // Генерация qrcode
    const qrcode =  qr.toFile(
      `./public/uploads/orders/${results.id}.png`,
      `${results.id}`,
      {},
      () => null
    )
    // ================
    await strapi.services.order.update({ id: results.id }, {
      qrcode_url: `/uploads/orders/${results.id}.png`
    })
  },

  computeComission (data) {
    data.commission = data.price * 0.1
  },

  // Создание уведомления продавцу (гиду) о новой покупке
  async createNewOrderNotification (results, data) {
    let profile, pushProfile
    try {
      profile = await strapi.services.profile.findOne({ _id: results.seller_id })
      pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
        params: {
          _uid: profile.uid
        }
      }).then(res => res.data[0] || null)
    } catch (err) {
      console.log('Profile for push not found...', err)
    }

    try {
      await axios
        .get(
          `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
          {
            params: {
              _cycle: "orderCreated",
              _locale: profile.locale
            }
          }
        )
        .then((res) => {
          res.data.forEach(item => {
            axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
              title: item.title,
              body: item.body,
              image: item.image,
              link: item.link,
              profile: pushProfile.id
            })
          })
        });
    } catch (e) {
      throw e
    }

  },


  async createSellerConfirmedNotification (params, data) {
    let order, profile, pushProfile, secondProfile, secondPushProfile
    try {
      order = await strapi.services.order.findOne({ id: params._id })
    } catch (err) {
      console.log('order | services | createSellerConfirmedNotification | Order undefined... ' + err.message)
    }

    // Создание уведомления туристу после подтверждения ордера гидом
    if (!order.seller_confirmed && data.seller_confirmed) {
      // Получаем профиль для пуш уведомления
      try {
        profile = await strapi.services.profile.findOne({ id: data.profile })
        pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
          params: {
            _uid: profile.uid
          }
        }).then(res => res.data[0] || null)
      } catch (err) {
        console.log('order | services | createSellerConfirmedNotification | pushProfile undefined... ' + err.message)
      }

      // Оптравляем цикличное пуш уведомление
      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: 'orderSellerApproved',
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (err) {
        console.log('order | services | createSellerConfirmedNotification | Notification is not send... ' + err.message)
      }
    }

    // Создание уведомления туристу и гиду после отмены ордера
    if (!order.canceled && data.canceled){

      // Получаем гида
      try {
        profile = await strapi.services.profile.findOne({ id: data.seller_id })
        pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
          params: {
            _uid: profile.uid
          }
        }).then(res => res.data[0] || null)
      } catch (e) {
        throw e
      }
      // Оптравляем цикличное пуш уведомление
      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: 'orderCancelled',
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (err) {
        console.log('order | services | createSellerConfirmedNotification | Notification is not send... ' + err.message)
      }

      // Получаем туриста
      try {
        profile = await strapi.services.profile.findOne({ id: data.profile })
        pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
          params: {
            _uid: profile.uid
          }
        }).then(res => res.data[0] || null)
      } catch (e) {
        throw e
      }
      // Оптравляем цикличное пуш уведомление
      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: 'orderCancelled',
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (err) {
        console.log('order | services | createSellerConfirmedNotification | Notification is not send... ' + err.message)
      }
    }
  }
};
