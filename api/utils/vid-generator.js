export default function vidGenerator() {
  const date = new Date
  const vid = `${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getMinutes()}${date.getMilliseconds()}`
  return vid
}
