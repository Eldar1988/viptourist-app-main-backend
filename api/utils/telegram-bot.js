const TelegramBot = require('node-telegram-bot-api')

const bot = {
  admin: {
    chatId: '-1001581331462',
    token: '5090533361:AAFGHMqcOmM3etdJiDPBUvu5x9DiqhcgByw',
    links: {
      guides: 'http://admin.viptourist.club/#/guides',
      tours: 'http://admin.viptourist.club/#/tours',
      offers: 'http://admin.viptourist.club/#/offers',
      tourists: 'http://admin.viptourist.club/#/tourists'
    },

    sendMessage: function (type = '', id = '', text = '') {
      const testBot = new TelegramBot(this.token, { polling: false })

      if (type && id) {
        let name = ''

        switch (type) {
          case 'tours':
            name = 'тур'
            break
          case 'offers':
            name = 'оффер'
            break
          case 'guides':
            name = 'гид'
            break
          case 'tourists':
            name = 'турист'
            break
          default:
            name = 'объект'
            break
        }

        const message = text || `Создан новый ${name}:`

        testBot.sendMessage(this.chatId, `${message} ${this.links[type]}/${id}`).then(() => console.log('Telegram message send...'))
      }
      else {
        testBot.sendMessage(this.chatId, text).then(() => console.log('Telegram message send...'))
      }
    }
  },

  finance: {
    chatId: '-1001243828294',
    token: '5084860299:AAEhzLKkQfQXJ7fyz5LZyPrKjTzCi96cNiY',
    links: {
      orders: 'http://finance.viptourist.club/#/orders',
      payments: 'http://finance.viptourist.club/#/payments'
    },

    sendMessage: function (type = '', id = '', text = '') {
      const testBot = new TelegramBot(this.token, { polling: false })
      testBot.sendMessage(this.chatId, `${text} ${this.links[type]}/?id=${id}`).then(() => console.log('Telegram message send...'))
    }
  },

  translate: {
    chatId: '-1001748161164',
    token: '5068894107:AAGQoNqiadS5Lj5IPUZ1MCQxpLt6mYhyCK0',
    links: {
      tours: 'http://translate.viptourist.club/#/tours',
      offers: 'http://translate.viptourist.club/#/offers',
    },

    sendMessage: function (type = '', id = '', text = '') {
      const testBot = new TelegramBot(this.token, { polling: false })
      testBot.sendMessage(this.chatId, `${text} ${this.links[type]}`).then(() => console.log('Telegram message send...'))
    }
  }
}

module.exports = bot
