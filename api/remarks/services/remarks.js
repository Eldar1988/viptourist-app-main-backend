"use strict";
const axios = require("axios");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  // Создание пуш уведомления (цикличное)
  async createNotification(result, data) {
    let profile, pushProfile
    try {
      profile = await strapi.services.profile.findOne({ _id: result.tour.profile })
      pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
        params: {
          _uid: profile.uid
        }
      }).then(res => res.data[0] || null)
    } catch (err) {
      throw err
    }


    try {
      await axios
      .get(
        `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
        {
          params: {
            _cycle: "remarkCreated",
            _locale: profile.locale
          }
        }
      )
      .then((res) => {
        res.data.forEach(item => {
          axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
            title: item.title,
            body: item.body,
            image: item.image,
            link: item.link,
            profile: pushProfile.id
          })
        })
      });
    } catch (err) {
      throw err
    }
  },
};
