'use strict';
const bot = require('../../utils/telegram-bot')
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(results, data) {
      bot.finance.sendMessage('payments', results.id, 'Новая заявка на выплату: ')
    }
  }
};
