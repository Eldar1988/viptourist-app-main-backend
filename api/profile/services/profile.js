"use strict";
const axios = require("axios");
const profile = require("../models/profile");
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-services)
 * to customize this service
 */

module.exports = {
  async createProfileCopyForNotifications(result, data) {
    try {
      await axios.post(
        `http://${process.env.HOST}:1338/profiles`,
        {
          ...data,
        },
        {}
      );
    } catch (e) {
      throw e;
    }
  },

  async updateProfileCopyForNotifications(result, params, data) {
    let profile = {};

    try {
      profile = await axios
        .get(`http://${process.env.HOST}:1338/profiles/?_uid=${result.uid}`)
        .then((res) => res.data[0] || null);

      if (profile) {
        await axios.put(
          `http://${process.env.HOST}:1338/profiles/${profile._id}`,
          {
            ...profile,
            ...data,
            notifications: profile.notifications,
          }
        );
      }
    } catch (e) {
      throw e;
    }
  },

  async createProfileVerifiedNotification(params, data) {
    let profile, pushProfile

    // Получаем профиль
    try {
      profile = await strapi.services.profile.findOne({ id: params._id })
    } catch (err) {
      throw err
    }

    // Если статус профиля сменился на верифицированный оптравляем пуш уведомление
    if (!profile.is_verified && data.is_verified) {
      // Получаем профиль для пуш уведомления
      try {
        pushProfile = await axios.get(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/profiles`, {
          params: {
            _uid: profile.uid
          }
        }).then(res => res.data[0] || null)
      } catch (err) {
        throw err
      }

      // Отправляем уведомление
      try {
        await axios
          .get(
            `http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/general-notifications`,
            {
              params: {
                _cycle: "profileVerified",
                _locale: profile.locale
              }
            }
          )
          .then((res) => {
            res.data.forEach(item => {
              axios.post(`http://${process.env.HOST}:${process.env.NOTIFICATIONS_PORT}/notifications`, {
                title: item.title,
                body: item.body,
                image: item.image,
                link: item.link,
                profile: pushProfile.id
              })
            })
          });
      } catch (err) {
        throw err
      }
    }


  }
};
