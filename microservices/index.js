const express = require('express')
const cors = require('cors')
require('dotenv').config()

const myApp = require('./app')


const app = express()
const { APP_PORT, APP_IP, APP_PATH } = process.env
app.use(cors())
app.use(express.json({ limit: '10mb' }))

app.post('/translate', async (req, res) => await myApp.translator.getTranslate(req, res))
app.post('/weather', async (req, res) => await myApp.weather.getWeather(req, res))
app.get('/course', (req, res) => myApp.courses.getCourses(req, res))


app.listen(APP_PORT, APP_IP, () => {
  console.log('server started...', APP_PORT, APP_IP)
})
