const axios = require('axios')

const apiKey = '324fe96ab359abec70ad9f9e5a497b6c'

async function getWeather (req, res) {
  const city = req.body.city

  try {
    let lat, lon
    await axios.get(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=5&appid=${apiKey}`).then(wres => {
      const data = wres.data?.[0] || {}
      lat = data.lat
      lon = data.lon
    })

    await axios
      .get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric`)
      .then(wres => {
        res.json({ data: wres.data })
      })
  } catch (e) {
    res.status(500)
    res.json({ error: e })
  }
}


module.exports = {
  getWeather
}
