const axios = require('axios')


const getCourses = async (req, res) => {
  const {base, quote} = req.query
  if (!base) {
    res.status(500)
    res.json({error: 'base is required query param'})
  }
  if (!quote) {
    res.status(500)
    res.json({error: 'quote is required query param'})
  }

  try {
    await axios
      .get(`https://api.coingate.com/v2/rates/merchant/${base}/${quote}`)
      .then(r => {
        res.json({
          baseCurrency: base,
          quoteCurrency: quote,
          price: r.data
        })
      })
  } catch (err) {
    res.status(500)
    res.json({message: err.message, error: err})
  }
}

module.exports = {
  getCourses
}
