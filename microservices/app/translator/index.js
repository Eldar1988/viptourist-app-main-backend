const { Translate } = require("@google-cloud/translate").v2;


class Translator {
  key = "AIzaSyByQhNY4yuZa4GiMCSIZP8uIznijKFYf5Q";
  translate = new Translate({ key: this.key });
}

async function getTranslate (req, res) {
  const translator = new Translator()
  const {text, target} = req.body

  try {
    const translations = await translator.translate.translate(text, target.split('-')[0])
    res.json(translations)
  } catch (e) {
    res.status(500)
    res.json({ error: e })
  }
}

module.exports = {
  getTranslate
}
