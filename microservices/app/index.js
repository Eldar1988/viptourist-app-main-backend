const translator = require('./translator')
const weather = require('./weather')
const courses = require('./currency')

module.exports = {
  translator,
  weather,
  courses
}
